﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Iks_Oks
{
    public partial class IksOks : Form
    {

        public int a = 0, b = 0;            //dvi varijable za praćenje rezultata

        public IksOks()
        {
            InitializeComponent();
        }

        public void Reset()                 //funkcija reset koja resetira sve buttone u svrhu pokretanja nove igre
        {
            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";
        }

        public void Check()                 //funkcija check koja provjerava je li doslo do 3 uzastopna znaka na polju
        {
            if (((button1.Text == button2.Text) && (button2.Text == button3.Text) && (button3.Text == "X")) ||
                ((button1.Text == button4.Text) && (button4.Text == button7.Text) && (button7.Text == "X")) ||
                ((button1.Text == button5.Text) && (button5.Text == button9.Text) && (button9.Text == "X")) ||
                ((button3.Text == button6.Text) && (button6.Text == button9.Text) && (button9.Text == "X")) || //if petlja koja provjerava stupce, retke i dijagonale u svrhu trazenja pobjednika
                ((button3.Text == button5.Text) && (button5.Text == button7.Text) && (button7.Text == "X")) ||
                ((button7.Text == button8.Text) && (button8.Text == button9.Text) && (button9.Text == "X")) ||
                ((button2.Text == button5.Text) && (button5.Text == button8.Text) && (button8.Text == "X")) ||
                ((button4.Text == button5.Text) && (button5.Text == button6.Text) && (button6.Text == "X")))
            {
                MessageBox.Show("Congratulations " + lbl_PL1.Text + "!! You won this round!!");                //Message box za pobjedu jedne runde
                a++;                                           //povećanje rezultata
                label4.Text = a.ToString();                                     //upis rezultata na labelu
                if (a == 3)                                         //ako je rezultat 3, doslo je do kraja igre, poziva se funckija New_Game
                {
                    MessageBox.Show("Congratulations " + lbl_PL1.Text + "!! You won the game!!");
                    New_Game();
                }
                
                 else
                    Reset();                             //reset ako je doslo do pobjede runde, ali ne i igre
            }

                        // isto kao i prva if petlja samo sto se ovaj put provjerava drugi igrac
            if (((button1.Text == button2.Text) && (button2.Text == button3.Text) && (button3.Text == "O")) ||
                ((button1.Text == button4.Text) && (button4.Text == button7.Text) && (button7.Text == "O")) ||
                ((button1.Text == button5.Text) && (button5.Text == button9.Text) && (button9.Text == "O")) ||
                ((button3.Text == button6.Text) && (button6.Text == button9.Text) && (button9.Text == "O")) ||
                ((button3.Text == button5.Text) && (button5.Text == button7.Text) && (button7.Text == "O")) ||
                ((button7.Text == button8.Text) && (button8.Text == button9.Text) && (button9.Text == "O")) ||
                ((button2.Text == button5.Text) && (button5.Text == button8.Text) && (button8.Text == "O")) ||
                ((button4.Text == button5.Text) && (button5.Text == button6.Text) && (button6.Text == "O")))
            {
                MessageBox.Show("Congratulations " + lbl_PL2.Text + "!! You won this round!!");
                b++;
                label5.Text = b.ToString();
                if (b == 3)
                {
                    MessageBox.Show("Congratulations " + lbl_PL2.Text + "!! You won the game!!");
                    New_Game();
                }
                else
                    Reset();
            }

                //if za nerjesenu situaciju
            if( button1.Text!="" &&
                button2.Text!="" &&
                button3.Text!="" &&
                button4.Text!="" &&
                button5.Text!="" &&
                button6.Text!="" &&
                button7.Text!="" &&
                button8.Text!="" &&
                button9.Text!="" )
            {
                MessageBox.Show("A Tie! Reseting the field!!"); //poruka korisniku
                Reset();                                        //resetiranje svih buttona
            }
        }

        public void New_Game()                 //funkcija za pokretanje nove igre
        {
            Reset();                        
            lbl_PL1.Text = "";
            lbl_PL2.Text = "";
            label4.Text = "0";
            label5.Text = "0";
            a = 0;
            b = 0;

        }


        private void Ok_Click(object sender, EventArgs e)       //klikom na ok button upisuje se ime igraca
        {
            if (lbl_PL1.Text == "")                      //porvjeravamo da li je vec uneseno ime prvog igraca                                              
            {
                lbl_PL1.Text = tb_Ime.Text;                //unos imena prvog igraca  ( ispis na label )
                tb_Ime.Text = "";                        //brise teks iz text boxa
            }
            else if (lbl_PL2.Text == "")                //provjeravamo da li je vec uneseno ime drugog igraca
            {
                lbl_PL2.Text = tb_Ime.Text;             //unos imena drugog igraca
                lbl_NowPlaying.Text = lbl_PL1.Text;     //postavljanje trenutnog igraca na prvog igraca
                tb_Ime.Text = "";                       //brisanje teksta iz text boxa
            }
            else            //u slucaju da je su oba imena vec upisana
            {
                MessageBox.Show("Two players already playing!!");
                tb_Ime.Text = "";       //brisanje teksta iz text boxa
            }
        }

       
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "" )                     //svi buttoni krecu bez teksta, pa se zbog toga provjerava da li ima teks na buttonu prilikom clicka na taj button
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text) //ako nema teksta onda provjeravamo koji je igrac na potezu
                {
                    button1.Text = "X";         //ispis znaka X na button
                    lbl_NowPlaying.Text = lbl_PL2.Text; //promjena trenutnog igraca
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)   //provjera igraca
                {
                    button1.Text = "O";             //ispis znaka O na button
                    lbl_NowPlaying.Text = lbl_PL1.Text; //promjena trenutnog igraca
                }

                else
                    MessageBox.Show("Enter player names!"); //ako label koji prkazuje trenutnog igraca nije popunjen trazi od korisnika da ispuni imena
               
                Check(); //poziv funkcije Check
            }
            else                   //u slucaju da ima teksta na buttonu javlja poruku korisniku da je to polje vec odigrano
                MessageBox.Show("This field has already been played!\n       Please select another field!");
        }
            

            //svi ostali buttoni su isti kao i prvi button
        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button2.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button2.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (button3.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button3.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button3.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (button4.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button4.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button4.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (button5.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button5.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button5.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (button6.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button6.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button6.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (button7.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button7.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button7.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (button8.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button8.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button8.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        private void button9_Click(object sender, EventArgs e)

        {
            if (button9.Text == "")
            {
                if (lbl_NowPlaying.Text == lbl_PL1.Text)
                {
                    button9.Text = "X";
                    lbl_NowPlaying.Text = lbl_PL2.Text;
                }
                else if (lbl_NowPlaying.Text == lbl_PL2.Text)
                {
                    button9.Text = "O";
                    lbl_NowPlaying.Text = lbl_PL1.Text;
                }

                else
                    MessageBox.Show("Enter player names!");
                Check();
            }
            else
                MessageBox.Show("This field has already been played!\n Please select another field!");
        }

        
            
        private void btn_Exit_Click(object sender, EventArgs e)     //button exit izlazi iz aplikacije
        {
            Application.Exit();
        }

        private void btn_NewGame_Click(object sender, EventArgs e)  //button new game pokrece novu igru u slucaju da je potrebno
        {
            New_Game();
        }

        private void tb_Ime_KeyDown(object sender, KeyEventArgs e)     //event za text box
        {
            if (e.KeyCode == Keys.Enter)            //provjeravamo je li stisnta tipka Enter
            {
                Ok_Click(this, new EventArgs());       //ako jeste, omogućujemo upis imena iz text boxa pritiskom tipke enter umjesto pritiskanja na ok button tako sto pozivamo Ok_Click event
            }
        }

        private void IksOks_Load(object sender, EventArgs e)        //nema neke svrhe, slucajno dva puta kliknuo na formu i sada ne mogu izbrisati load
        {

        }



    }
}
